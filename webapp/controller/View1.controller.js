sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function(Controller, MessageToast, MessageBox) {
	"use strict";

	return Controller.extend("NT.James.Hello.controller.View1", {
		MesPress: function(oEvent) {
			var oItem = oEvent.getSource();
			//console.log(oEvent.getSource());
			//MessageToast.show("ok");
			MessageToast.show(oItem.getText());
		},

		MesPop: function() {
			sap.m.MessageBox.alert("Press.X to close", {
				title: "HelloUI5", // default
				styleClass: "", // default
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				textDirection: sap.ui.core.TextDirection.Inherit // default
			});
		}
	});

});